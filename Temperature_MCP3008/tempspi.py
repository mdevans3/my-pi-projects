#!/usr/bin/env python
import time
import os
import RPi.GPIO as GPIO
import eeml
import spidev
import urllib2

GPIO.setmode(GPIO.BCM)
DEBUG = 0
LOGGER = 1
SLEEP_TIME = 30
heaterIsOn = 0

spi = spidev.SpiDev()
spi.open(0,0)

# read SPI data from MCP3008 chip, 8 possible adc's (0 thru 7)
def readadc(adcnum):
        if ((adcnum > 7) or (adcnum < 0)):
                return -1
        r = spi.xfer2([1,(8+adcnum)<<4,0])
        adcout = ((r[1]&3) << 8) + r[2]
        return adcout

def temperature(read_adc):
        # convert analog reading to millivolts = ADC * ( 3300 / 1024 )
        millivolts = read_adc * ( 3300.0 / 1024.0) # ^^ possibly should be 1000 not 1024

        # 10 mv per degree
	    temp_C = ((millivolts - 100.0) / 10.0) - 40

        # convert celsius to fahrenheit
        temp_F = ( temp_C * 9.0 / 5.0 ) + 32

        # remove decimal point from millivolts
        millivolts = "%d" % millivolts

        # show only one decimal place for temprature and voltage readings
        temp_C = "%.1f" % temp_C
        temp_F = "%.1f" % temp_F
        
        temp = { 'C': float(temp_C), 'F': float(temp_F), 'millivolts': int(millivolts) }        
        return temp

# set up the LED pins
GPIO.setup(21, GPIO.OUT)

# COSM variables. The API_KEY and FEED are specific to your COSM account and must be changed
API_KEY = 'VBUkMRI4pwIUbU02Idzhfl95ol-SAKw2Rm94V3BoWm9rcz0g'
FEED = 80773

API_URL = '/v2/feeds/{feednum}.xml' .format(feednum = FEED)

# temperature sensor connected channel 0 & 1 of mcp3008
adcnum0 = 0
adcnum1 = 0

while True:
        # read the analog pin (temperature sensor LM35)
        read_adc0 = readadc(adcnum0)
	    time.sleep(0.05)
        read_adc0 = readadc(adcnum0)
        time.sleep(0.05)
        read_adc1 = read_adc0 #readadc(adcnum1)

        #GPIO.output(21, True)

        temp_0 = temperature(millivolts)
        temp_1 = temperature(millivolts)

        avg_C = "%.1f" % ((temp_0['C'] + temp_1['C']) / 2)
        avg_F = "%.1f" % ((temp_0['F'] + temp_1['F']) / 2)
        avg = { 'C': float(avg_C), 'F': float(avg_F) }

        if ((avg['C'] < 21) and not (heaterIsOn)):
            f = urllib2.urlopen('http://kosh/~mark/iPhoneHome/scripts/x10.php?action=on_off&id=B10&state=on')
            if f.read() == 'OK':
                heaterIsOn = 1

        if ((avg['C'] > 24) and (heaterIsOn)):
            f = urllib2.urlopen('http://kosh/~mark/iPhoneHome/scripts/x10.php?action=on_off&id=B10&state=off')
            if f.read() == 'OK':
                heaterIsOn = 0

        if DEBUG:
                print "----------------------------------------"
                print "read_adc0:\t", read_adc0
                print "millivolts0:\t", temp_0['millivolts']
                print "temp_C0:\t\t", temp_0['C']
                print "temp_F0:\t\t", temp_0['F']
                print "read_adc1:\t", read_adc1
                print "millivolts1:\t", temp_1['millivolts']
                print "temp_C1:\t\t", temp_1['C']
                print "temp_F1:\t\t", temp_1['F']
                print "Average C:\t", avg['C']
                print "Average F:\t", avg['F']

        if LOGGER:
                try:    
                    # open up your cosm feed
                    pac = eeml.Pachube(API_URL, API_KEY)

                    #send celsius data
                    pac.update([eeml.Data(0, avg['C'], unit=eeml.Celsius())])

                    #send fahrenheit data
                    pac.update([eeml.Data(1, avg['F'], unit=eeml.Fahrenheit())])

                    #send Heater State data
                    pac.update([eeml.Data(2, heaterIsOn, unit=eeml.RH())])

                    # send data to cosm
                    pac.put()
                except:
			        print "EEML Error"

        #GPIO.output(21, False)
        # hang out and do nothing for 10 seconds, avoid flooding cosm
        time.sleep(SLEEP_TIME)

