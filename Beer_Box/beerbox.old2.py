#!/usr/bin/env python
import time
import os
import RPi.GPIO as GPIO
import eeml
import urllib2
import socket
import sys
import simplejson as json

GPIO.setmode(GPIO.BCM)
DEBUG = 1
LOGGER = 1
HEATING = 0
SLEEP_TIME = 30
ANALOG_DELAY = 0.05
heaterIsOn = 0
temp_profile = { 'min': 21, 'max': 24 }

#temp_0 = { 'C': 0.0, 'F': 0.0, 'millivolts': 0 } 
#temp_1 = { 'C': 0.0, 'F': 0.0, 'millivolts': 0 } 
#temp_2 = { 'C': 0.0, 'F': 0.0, 'millivolts': 0 } 

# read SPI data from MCP3008 chip, 8 possible adc's (0 thru 7)
def readadc(adcnum, clockpin, mosipin, misopin, cspin):
        if ((adcnum > 7) or (adcnum < 0)):
                return -1
        GPIO.output(cspin, True)

        GPIO.output(clockpin, False)  # start clock low
        GPIO.output(cspin, False)     # bring CS low

        commandout = adcnum
        commandout |= 0x18  # start bit + single-ended bit
        commandout <<= 3    # we only need to send 5 bits here
        for i in range(5):
                if (commandout & 0x80):
                        GPIO.output(mosipin, True)
                else:
                        GPIO.output(mosipin, False)
                commandout <<= 1
                GPIO.output(clockpin, True)
                GPIO.output(clockpin, False)

        adcout = 0
        # read in one empty bit, one null bit and 10 ADC bits
        for i in range(12):
                GPIO.output(clockpin, True)
                GPIO.output(clockpin, False)
                adcout <<= 1
                if (GPIO.input(misopin)):
                        adcout |= 0x1

        GPIO.output(cspin, True)

        adcout /= 2       # first bit is 'null' so drop it
        return adcout
        
def analogRead(adcnum):
    l = []
    
    while float(len(l)) < 20:
        read0 = readadc(adcnum, SPICLK, SPIMOSI, SPIMISO, SPICS)
        time.sleep(ANALOG_DELAY)
        l.append(read0)

    l.sort()
    for i in range(1, 6):
        l.pop()
    
    l.reverse()
    for i in range(1, 6):
        l.pop()
    
    #print l
    #print float(len(l))
    read = sum(l) / float(len(l))
    #print read
    return read

def temperature(read_adc):
    # convert analog reading to millivolts = ADC * ( 3300 / 1024 )
    millivolts = read_adc * ( 3300.0 / 1024.0)

    # 10 mv per degree
    temp_C = ((millivolts - 100.0) / 10.0) - 40
    # temp_C = (millivolts - 500.0) / 10.0

    # convert celsius to fahrenheit
    temp_F = ( temp_C * 9.0 / 5.0 ) + 32

    # remove decimal point from millivolts
    millivolts = "%d" % millivolts

    # show only one decimal place for temprature and voltage readings
    temp_C = "%.1f" % temp_C
    temp_F = "%.1f" % temp_F
        
    temp = { 'C': float(temp_C), 'F': float(temp_F), 'millivolts': int(millivolts) }        
    return temp

def debug_print():
        print "----------------------------------------"
        print "read_adc0:\t", read_adc0
        print "millivolts0:\t", temp_0['millivolts']
        print "temp_C0:\t\t", temp_0['C']
        print "temp_F0:\t\t", temp_0['F']
        print "read_adc1:\t", read_adc1
        print "millivolts1:\t", temp_1['millivolts']
        print "temp_C1:\t\t", temp_1['C']
        print "temp_F1:\t\t", temp_1['F']
        print "Average C:\t", avg['C']
	print "Average F:\t", avg['F']

def do_logger():
        try:    
            # open up your cosm feed
            pac = eeml.Pachube(API_URL, API_KEY)

            #send celsius data
            pac.update([eeml.Data(0, temp_0['C'], unit=eeml.Celsius())])

            #send fahrenheit data
            pac.update([eeml.Data(1, temp_0['F'], unit=eeml.Fahrenheit())])

            #send celsius data
            pac.update([eeml.Data(2, temp_1['C'], unit=eeml.Celsius())])

            #send fahrenheit data
            pac.update([eeml.Data(3, temp_1['F'], unit=eeml.Fahrenheit())])

            #send celsius data
            pac.update([eeml.Data('Outside_Air', temp_2['C'], unit=eeml.Celsius())])

            # send data to cosm
            pac.put()
        except:
	        print "EEML Error"


# change these as desired - they're the pins connected from the
# SPI port on the ADC to the Cobbler
SPICLK = 18
SPIMISO = 23
SPIMOSI = 24
SPICS = 25

# set up the SPI interface pins
GPIO.setup(SPIMOSI, GPIO.OUT)
GPIO.setup(SPIMISO, GPIO.IN)
GPIO.setup(SPICLK, GPIO.OUT)
GPIO.setup(SPICS, GPIO.OUT)

# set up the LED pins
GPIO.setup(22, GPIO.OUT)

# COSM variables. The API_KEY and FEED are specific to your COSM account and must be changed
API_KEY = '7Aoop44HCS-qfqgPHYWYtYQ1WMCSAKxVT1RrM1Q0NmNmVT0g'
FEED = 76692

API_URL = '/v2/feeds/{feednum}.xml' .format(feednum = FEED)

# temperature sensor connected channel 0 & 1 of mcp3008
adcnum0 = 0 # Top sensor
adcnum1 = 1 # Bottom sensor
adcnum2 = 2

def BeerReadings():
    
    global temp_0
    global temp_1
    global temp_2
    global read_adc0
    global read_adc1
    global read_adc2
    global avg
    global avg_C
    global avg_F
    
    # read the analog pin (temperature sensor LM35)
    read_adc0 = analogRead(adcnum0)
    read_adc1 = analogRead(adcnum1)
    read_adc2 = analogRead(adcnum2) #readadc(adcnum1, SPICLK, SPIMOSI, SPIMISO, SPICS)

    #GPIO.output(22, True)

    temp_0 = temperature(read_adc0)
    temp_1 = temperature(read_adc1)
    temp_2 = temperature(read_adc2)

    avg_C = "%.1f" % ((temp_0['C'] + temp_1['C']) / 2)
    avg_F = "%.1f" % ((temp_0['F'] + temp_1['F']) / 2)
    avg = { 'C': float(avg_C), 'F': float(avg_F) }

    if HEATING:
        if ((temp_0['C'] < temp_profile['min']) and not (heaterIsOn)):
            f = urllib2.urlopen('http://kosh/~mark/iPhoneHome/scripts/zwave.php?action=on_off&id=5&state=1')
            # f = urllib2.urlopen('http://192.168.1.133:3480/data_request?id=lu_action&output_format=xml&DeviceNum=10&serviceId=urn:upnp-org:serviceId:SwitchPower1&action=SetTarget&newTargetValue=1')
            if f.read() == 'OK':
                heaterIsOn = 1

        if ((temp_0['C'] > temp_profile['max']) and (heaterIsOn)):
            f = urllib2.urlopen('http://kosh/~mark/iPhoneHome/scripts/zwave.php?action=on_off&id=5&state=0')
            # f = urllib2.urlopen('http://192.168.1.133:3480/data_request?id=lu_action&output_format=xml&DeviceNum=10&serviceId=urn:upnp-org:serviceId:SwitchPower1&action=SetTarget&newTargetValue=0')
            if f.read() == 'OK':
                heaterIsOn = 0

    if DEBUG:
	debug_print()

    if LOGGER:
	do_logger()

    #GPIO.output(22, False)
    # hang out and do nothing for 10 seconds, avoid flooding cosm
            
    return True
    
# user-accessible port
PORT = 8037

# establish server
service = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
service.bind(("", PORT))
service.setblocking(1)  # set socket functions to be blocking
service.listen(5)  # Create a backlog queue for up to 5 connections
service.settimeout(float(SLEEP_TIME))

print "listening on port", PORT

run = 1

while(run):
    # serve forever
    try:
    	channel, info = service.accept()
    	print "connection from", info

    	message = channel.recv(1024).decode('utf-8')

	if "=" in message:
		messageType, value = message.split("=", 1)
	else:
		messageType = message

	if messageType == "ack":
		channel.send('ack')
	elif messageType == "setTemp":
		old_temp = temp
		temp = value
		channel.send("Old:" + str(old_temp) + " / New:" + str(temp))
	elif messageType == "getTemp":
		channel.send(str(temp_0['C']) + "," + str(temp_1['C']) + "," + str(temp_2['C']))
	elif messageType == "jsonTemp":
		temp_all = { 'temp_0': temp_0, 'temp_1': temp_1, 'temp_2': temp_2 }  
		channel.send(json.dumps(temp_all))
	elif messageType == "stopScript":
		#channel.send('Stopping...') # send timestamp
		print >> sys.stderr, (time.strftime("%b %d %Y %H:%M:%S    ") +
						"Stopping: User initiated will not auto restart")
		run = 0
		continue
	else:
		print >> sys.stderr, (time.strftime("%b %d %Y %H:%M:%S    ") +
						"Error: Received invalid message on socket: " + message)
    	channel.shutdown(socket.SHUT_RDWR)

    except socket.timeout:
	    BeerReadings();

    except socket.error, e:
	    print e

channel.shutdown(socket.SHUT_RDWR)  # close socket
channel.close()
