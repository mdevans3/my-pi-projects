import os
import glob
import time
import eeml
import RPi.GPIO as GPIO, time, os      
 
GPIO.setmode(GPIO.BCM)

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

t = time.time()

# COSM variables. The API_KEY and FEED are specific to your COSM account and must be changed
API_KEY = 'xgEoN8Sfz_xHfJzm8l4yZAST4nySAKxNdlh6MlE2ZVpBRT0g'
FEED = 76365
API_URL = '/v2/feeds/{feednum}.xml' .format(feednum = FEED)

def read_temp_raw(device_file):
	f = open(device_file, 'r')
	lines = f.readlines()
	f.close()
	return lines

def read_temp(df):
	lines = read_temp_raw(df)
	while lines[0].strip()[-3:] != 'YES':
		time.sleep(0.2)
		lines = read_temp_raw(df)
	equals_pos = lines[1].find('t=')
	if equals_pos != -1:
		temp_string = lines[1][equals_pos+2:]
		temp_c = float(temp_string) / 1000.0
		temp_f = temp_c * 9.0 / 5.0 + 32.0
		return temp_c, temp_f
        
def RCtime (RCpin):
    reading = 0
    GPIO.setup(RCpin, GPIO.OUT)
    GPIO.output(RCpin, GPIO.LOW)
    time.sleep(0.1)
 
    GPIO.setup(RCpin, GPIO.IN)
    # This takes about 1 millisecond per loop cycle
    while (GPIO.input(RCpin) == GPIO.LOW):
            reading += 1
    return reading

def readLight():
    l = []
    while float(len(l)) < 6:
        l.append(RCtime(23))

    l.sort()
    for i in range(1, 2):
        l.pop()

    avg = sum(l) / float(len(l))
    
    return avg

def cosmLog(avg):
	try:
		# open up your cosm feed
		pac = eeml.Pachube(API_URL, API_KEY)

		#send celsius data
		pac.update([eeml.Data(0, avg['C'], unit=eeml.Celsius())])

		#send fahrenheit data
		pac.update([eeml.Data(1, avg['F'], unit=eeml.Fahrenheit())])

    	#send fahrenheit data
		pac.update([eeml.Data(2, avg['L'])])
        
		# send data to cosm
		pac.put()
	except:
		print "EEML Error"

while True:
	epoch = int(time.time()) #int(time.mktime(time.strptime(time.time())))
	temp1_c, temp1_f = read_temp('/sys/bus/w1/devices/28-00000474f320/w1_slave')
	temp2_c, temp2_f = read_temp('/sys/bus/w1/devices/28-000004758057/w1_slave')
	light_reading = int(readLight())

	avg_C = "%.3f" % ((temp1_c + temp2_c) / 2)
	avg_F = "%.3f" % ((temp1_f + temp2_f) / 2)
	avg = { 'C': float(avg_C), 'F': float(avg_F), 'L': light_reading }

	#os.system('clear')
	#print time.asctime(time.localtime(time.time()))
	#print "1. Temp C: ", temp1_c, " | ", temp1_f
	#print "2. Temp C: ", temp2_c, " | ", temp2_f
	#print "Average:   ", avg['C'], " | ", avg['F']
	if epoch - t >= 60:
		#print "Logging..."
		cosmLog(avg)
		t = epoch
	time.sleep(1)

