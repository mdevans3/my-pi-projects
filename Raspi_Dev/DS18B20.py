import os
import glob
import time

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

t = time.time()

#base_dir = '/sys/bus/w1/devices/'
#device_folder = glob.glob(base_dir + '28*')[0]
#device_file = device_folder + '/w1_slave'

def read_temp_raw(device_file):
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

def read_temp(df):
    lines = read_temp_raw(df)
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw(df)
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        temp_f = temp_c * 9.0 / 5.0 + 32.0
        return temp_c, temp_f
	
while True:
    #print('1. ', read_temp('/sys/bus/w1/devices/28-0000048ef2e2/w1_slave'))
    #print('2. ', read_temp('/sys/bus/w1/devices/28-0000048e0456/w1_slave'))
    epoch = int(time.time()) #int(time.mktime(time.strptime(time.time())))
    temp1_c, temp1_f = read_temp('/sys/bus/w1/devices/28-00000474f320/w1_slave')
    #temp1_c, temp1_f = read_temp('/sys/bus/w1/devices/28-0000048ef2e2/w1_slave')
    temp2_c, temp2_f = read_temp('/sys/bus/w1/devices/28-000004758057/w1_slave')
    #print "Temp C: ", temp1_c, " | ", temp2_c, "\tTemp F: ", temp1_f, " | ", temp2_f, "\t", int(epoch - t)
    os.system('clear')
    print time.asctime(time.localtime(time.time()))
    print "1. Temp C: ", temp1_c, " | ", temp1_f
    print "2. Temp C: ", temp2_c, " | ", temp2_f
    if epoch - t >= 30:
        t = epoch
        #print "log..."
    time.sleep(1)

