#!/usr/bin/env python
import time
import os
import RPi.GPIO as GPIO
import eeml

GPIO.setmode(GPIO.BCM)
DEBUG = 0
LOGGER = 1

# read SPI data from MCP3008 chip, 8 possible adc's (0 thru 7)
def readadc(adcnum, clockpin, mosipin, misopin, cspin):
        if ((adcnum > 7) or (adcnum < 0)):
                return -1
        GPIO.output(cspin, True)

        GPIO.output(clockpin, False)  # start clock low
        GPIO.output(cspin, False)     # bring CS low

        commandout = adcnum
        commandout |= 0x18  # start bit + single-ended bit
        commandout <<= 3    # we only need to send 5 bits here
        for i in range(5):
                if (commandout & 0x80):
                        GPIO.output(mosipin, True)
                else:
                        GPIO.output(mosipin, False)
                commandout <<= 1
                GPIO.output(clockpin, True)
                GPIO.output(clockpin, False)

        adcout = 0
        # read in one empty bit, one null bit and 10 ADC bits
        for i in range(12):
                GPIO.output(clockpin, True)
                GPIO.output(clockpin, False)
                adcout <<= 1
                if (GPIO.input(misopin)):
                        adcout |= 0x1

        GPIO.output(cspin, True)

        adcout /= 2       # first bit is 'null' so drop it
        return adcout

def analogRead(adcnum):
    l = []
    
    while float(len(l)) < 20:
        read0 = readadc(adcnum, SPICLK, SPIMOSI, SPIMISO, SPICS)
        time.sleep(ANALOG_DELAY)
        l.append(read0)

	l.sort()
    for i in range(1, 6):
        l.pop()
    
    l.reverse()
    for i in range(1, 6):
        l.pop()
    
    #print l
    #print float(len(l))
    read = sum(l) / float(len(l))
    #print read
    return read

def temperature(read_adc):
        # convert analog reading to millivolts = ADC * ( 3300 / 1024 )
        millivolts = read_adc * ( 3300.0 / 1024.0) # ^^ possibly should be 1000 not 1024

        # 10 mv per degree
        temp_C = ((millivolts - 100.0) / 10.0) - 40

        # convert celsius to fahrenheit
        temp_F = ( temp_C * 9.0 / 5.0 ) + 32

        # remove decimal point from millivolts
        millivolts = "%d" % millivolts

        # show only one decimal place for temprature and voltage readings
        temp_C = "%.1f" % temp_C
        temp_F = "%.1f" % temp_F
        
        temp = { 'C': float(temp_C), 'F': float(temp_F), 'millivolts': int(millivolts) }        
        return temp

# change these as desired - they're the pins connected from the
# SPI port on the ADC to the Cobbler
SPICLK = 18
SPIMISO = 23
SPIMOSI = 24
SPICS = 25

# set up the SPI interface pins
GPIO.setup(SPIMOSI, GPIO.OUT)
GPIO.setup(SPIMISO, GPIO.IN)
GPIO.setup(SPICLK, GPIO.OUT)
GPIO.setup(SPICS, GPIO.OUT)

# set up the LED pins
GPIO.setup(22, GPIO.OUT)

# COSM variables. The API_KEY and FEED are specific to your COSM account and must be changed
API_KEY = 'xgEoN8Sfz_xHfJzm8l4yZAST4nySAKxNdlh6MlE2ZVpBRT0g'
FEED = 76365

API_URL = '/v2/feeds/{feednum}.xml' .format(feednum = FEED)

# temperature sensor connected channel 0 & 1 of mcp3008
adcnum0 = 0
adcnum1 = 1

while True:
        # read the analog pin (temperature sensor LM35)
        # read_adc0 = readadc(adcnum0, SPICLK, SPIMOSI, SPIMISO, SPICS)
        # read_adc1 = readadc(adcnum1, SPICLK, SPIMOSI, SPIMISO, SPICS)

        read_adc0 = analogRead(adcnum0)
        read_adc1 = analogRead(adcnum1)

        GPIO.output(22, True)

        temp_0 = temperature(read_adc0)
        temp_1 = temperature(read_adc1)

        avg_C = "%.1f" % ((temp_0['C'] + temp_1['C']) / 2)
        avg_F = "%.1f" % ((temp_0['F'] + temp_1['F']) / 2)
        avg = { 'C': float(avg_C), 'F': float(avg_F) }

        if DEBUG:
                print "----------------------------------------"
                print "read_adc0:\t", read_adc0
                print "millivolts0:\t", temp_0['millivolts']
                print "temp_C0:\t\t", temp_0['C']
                print "temp_F0:\t\t", temp_0['F']
                print "read_adc1:\t", read_adc1
                print "millivolts1:\t", temp_1['millivolts']
                print "temp_C1:\t\t", temp_1['C']
                print "temp_F1:\t\t", temp_1['F']
                print "Average C:\t", avg['C']
                print "Average F:\t", avg['F']

        if LOGGER:
                try:
                    # open up your cosm feed
                    pac = eeml.Pachube(API_URL, API_KEY)

                    #send celsius data
                    pac.update([eeml.Data(0, avg['C'], unit=eeml.Celsius())])

                    #send fahrenheit data
                    pac.update([eeml.Data(1, avg['F'], unit=eeml.Fahrenheit())])

                    # send data to cosm
                    pac.put()
                except:
    		        print "EEML Error"

        GPIO.output(22, False)
        # hang out and do nothing for 10 seconds, avoid flooding cosm
        time.sleep(30)

