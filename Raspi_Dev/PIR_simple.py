#!/usr/bin/python

import time
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
 
pir_pin = 23
 
GPIO.setup(pir_pin, GPIO.IN)
 
while True:
    if GPIO.input(pir_pin):
        print("PIR ALARM!")
    time.sleep(0.5)